﻿using System;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;

namespace Temper2
{
    public partial class frm_CloneRepository : Form
    {
        public frm_CloneRepository()
        {
            InitializeComponent();
        }

        private void Btn_clone_Click(object sender, EventArgs e)
        {
            try
            {
                this.CloneRepository();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        private void CloneRepository()
        {
            String repoUrl = txt_repositoryUrl.Text.Trim();
            String repoName = null;
            String directory = null;

            try
            {
                int begin = repoUrl.LastIndexOf("/") + 1;
                int end = repoUrl.LastIndexOf(".");
                int length = end - begin;
                repoName = repoUrl.Substring(begin, length);
                directory = UserPreferences.TemperPath + @"\" + repoName;
            }
            catch (Exception ex)
            {
                throw new Exception("Die Repository URL kann nicht geparst werden!");
            }

            if (Directory.Exists(directory))
            {
                DialogResult dialogResult = MessageBox.Show($"Der Ordner {repoName} existiert bereits! Soll der Ordner geöffnet werden?", "Temper", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    Process.Start(directory);
                    this.Close();
                }
 
                return;
            }

            this.Close();

            try
            {
                Process gitProcess = new Process
                {
                    StartInfo =
                    {
                        FileName = @"git",
                        Arguments = "clone " + repoUrl,
                        WorkingDirectory = UserPreferences.TemperPath,
                        UseShellExecute = true
                    }
                };

                gitProcess.Start();
                gitProcess.WaitForExit();
            }
            catch (Exception ex)
            {
                throw new Exception("Clonen des Repositories nicht möglich! Fehlendes Git?");
            }

            if (cbo_open.Checked)
            {
                Process.Start(directory);
            }
        }

        private void Txt_repositoryUrl_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                try
                {
                    this.CloneRepository();
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message);
                }
            }
        }
    }
}
